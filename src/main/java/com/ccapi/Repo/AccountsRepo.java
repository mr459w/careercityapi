package com.ccapi.Repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccapi.Entity.Accounts;

@Repository
public interface AccountsRepo extends JpaRepository<Accounts, Integer> {

}
