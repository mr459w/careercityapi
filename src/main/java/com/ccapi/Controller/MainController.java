package com.ccapi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ccapi.Entity.Accounts;
import com.ccapi.Service.AccountsService;
import com.ccapi.ServiceImpl.AccountsServiceImpl;

@CrossOrigin
@RestController
public class MainController {
	
	@Autowired
	private AccountsService accountsService;
	
	@CrossOrigin
	@GetMapping("/contactcenteraccounts")
	public List<Accounts> getAllAccounts(){
		return this.accountsService.getAllAccounts();
	}
	
	@CrossOrigin
	@PostMapping("/addaccount")
	public Accounts addAccount(@RequestBody Accounts accounts) {
		return this.accountsService.addAccount(accounts);
	}
}
