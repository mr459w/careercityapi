package com.ccapi.ServiceImpl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccapi.Entity.Accounts;
import com.ccapi.Repo.AccountsRepo;
import com.ccapi.Service.AccountsService;

@Service
@Transactional
public class AccountsServiceImpl implements AccountsService{
	
	@Autowired
	private AccountsRepo accountsRepo;

	@Override
	public List<Accounts> getAllAccounts() {
		// TODO Auto-generated method stub
		return this.accountsRepo.findAll();
	}

	@Override
	public Accounts getAccountById(Integer Id) {
		// TODO Auto-generated method stub
		return this.accountsRepo.findById(Id).get();
	}

	@Override
	public Accounts addAccount(Accounts accounts) {
		// TODO Auto-generated method stub
		return this.accountsRepo.save(accounts);
	}

	@Override
	public Accounts updateAccount(Accounts accounts) {
		// TODO Auto-generated method stub
		return this.accountsRepo.save(accounts);
	}

}
