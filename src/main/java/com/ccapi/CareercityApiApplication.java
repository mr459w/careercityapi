package com.ccapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CareercityApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CareercityApiApplication.class, args);
	}

}
