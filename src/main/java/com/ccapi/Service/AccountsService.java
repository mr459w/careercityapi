package com.ccapi.Service;

import java.util.List;

import com.ccapi.Entity.Accounts;

public interface AccountsService {
	
	public List<Accounts> getAllAccounts();
	public Accounts getAccountById (Integer Id);
	public Accounts addAccount(Accounts accounts);
	public Accounts updateAccount(Accounts accounts);
	

}
